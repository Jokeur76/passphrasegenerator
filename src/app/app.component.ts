import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Passphrase Generator';
  public enforce;
  public enforceDetails = 'En cochant cette option, vous aller modifier aléatoirement certains caratères par d\'autres ressemblant : s -> $, a -> 4, l -> |... Renforcant encore la sécurité de cette passphrase';
  private isLoaded = false;
  private db = [];
  public count = 0;
  public lastPassphrase = '';

  constructor(private http: HttpClient) {
    this.setDB();
  }

  generatePassphrase(wordNbr) {
    let code = '';
    let word;
    let passphrase = [];
    for (let i = 0; i < wordNbr; i++) {
      for (let i = 0; i < 5; i++) {
        code += this.getRandom(1, 6).toString();
      }
      console.log(code);
      passphrase.push(this.db.find(w => w.code === code).text);
      code = '';
    }
    if (this.enforce) {
      this.lastPassphrase = this.enforcePassphrase(this.lastPassphrase = passphrase.join(' '));
    } else {
      this.lastPassphrase = passphrase.join(' ');
    }
  }

  enforcePassphrase(passphrase) {
    const origin = ['a', 'e', 'i', 'o', 's', 'l', 'b', 't', 'z', ' '];
    const enforced = ['4', '3', '1', '0', '$', '|', '8', '7', '2', '_'];
    const minSuccess = this.getRandom(1, 100);
    let success;
    let index;
    let newPassphrase = '';

    for (const char of passphrase) {
      success = this.getRandom(1, 100);
      index = origin.indexOf(char);
      if (success >= minSuccess && index > -1) {
        newPassphrase += enforced[index];
      } else {
        newPassphrase += char;
      }
    }
    return newPassphrase;
  }

  setDB() {
    this.http.get('assets/diceware_passphrase_data.csv', {responseType: 'text'})
      .subscribe(
        datas => {
          const allTextLines = datas.split(/\r\n|\n/);
          let word;
          for (let line of allTextLines) {
            let data = line.split(';');
            if (data[0] !== '') {
              word = {
                _id: new Date().toISOString(),
                code: data[0],
                text: data[1]
              };
              this.db.push(word);
            }
            this.count++;
          }
          this.isLoaded = true;
        },
        error => {
          console.log(error);
        }
      );
  }

  getRandom(start, end) {
    return Math.floor((Math.random() * end) + start);
  }
}
